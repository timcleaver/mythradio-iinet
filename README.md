my-freezone-radio.rb
====================

my-freezone-radio is a simple script to scrape the [iiNet freezone radio web-page](freezone.iinet.net.au/radio)
and generate SQL that can be used to insert the iiNet free radio stations into a
[mythtv](www.mythtv.org) 0.26 [mythmusic](http://www.mythtv.org/wiki/MythMusic_Radio_Streaming)
install. This allows you to play the iiNet radio streams for free directly in your mythtv setup. Keep in mind
that the data used to listen to these radio streams is not counted against your monthly quota. Of course if you
are not an iiNet customer then this doesn't apply and the cost is dependent on your ISP.

Pre-requisites
--------------

This script relies on ruby and the following gems:

- hpricot
- open-uri
- awesome\_print

I have only tested using:

- operating system - [Fedora 16](www.fedoraproject.org)
- ruby - ruby 1.9.3p362 (2012-12-25 revision 38607) [x86\_64-linux]
- hpricot - 0.8.6
- awesome\_print - 1.1.0
- mythtv - 0.26.0-1.fc17

See [ruby](www.ruby-lang.org) and [gems](www.rubygems.org) for installation instructions for your environment.

Running
-------

To run, open a terminal and change to the directory that you downloaded
the script to. To execute the my-freezone-radio.rb ruby script type:

```bash
chmod +x my-freezone-radio.rb
./my-freezone-radio.rb
```

It will print the resulting SQL to the terminal. If you are satisfied with the output
then you can pipe the resulting SQL directly into the mythtv MySQL instance:

```bash
./my-freezone-radio.rb | mysql --user=$user --password=$password --host=$mythtv -vvv
```

where:

- $user - is the mythtv MySQL user (typically mythtv)
- $password - is the mythtv MySQL user password
- $host - is the i.p. address of the mythtv backend on which the MySQL instance is installed.

See the [MySQL documentation](dev.mysql.com/doc/) for more information.
