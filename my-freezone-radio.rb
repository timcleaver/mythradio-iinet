#!/usr/bin/env ruby

require 'rubygems'
require 'hpricot'
require 'open-uri'
require 'awesome_print'

# since we aren't using the database directly, we can't use prepared statements etc to
# sanitize our data. we use a simple quoter here and the user must check the output for
# incorrectly sanitized inputs
class String
  def sanitize
    gsub(/\\/, '\&\&').gsub(/'/, "''")
  end
end

# we add a unique constraint over the station and channel columns. this way we will pick up
# duplicates on multiple runs of this tool. we remove it afterwards so as not to cause the
# myth gui any grief
puts "CREATE UNIQUE INDEX station_channel ON mythconverg.music_radios (station, channel);"
doc = Hpricot open 'http://freezone.iinet.com.au/radio'
stations = (doc/'//div[@class="radio-items"]/article')
stations.each do |station|
  logo = (station/'div[@class="radio-table-icon noborder-left"]/a/img').first.attributes['src']
  title = (station/'div[@class="radio-table-details"]/h4/a[@class="radio-title"]').inner_html
  uri = (station/'div[@class="radio-table-status"]/p/a').first.attributes['href']
  genre = (station/'div[@class="radio-table-column"]/h4').inner_html
  puts "REPLACE INTO mythconverg.music_radios (
          station,
          channel,
          url,
          logourl,
          genre,
          metaformat,
          format
        ) VALUES (
          '#{title.sanitize}',
          '',
          '#{uri.sanitize}',
          '#{logo.sanitize}',
          '#{genre.sanitize}',
          '%a - %t',
          'cast'
        );"
end
puts "DROP INDEX station_channel ON mythconverg.music_radios;"

